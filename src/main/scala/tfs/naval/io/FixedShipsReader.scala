package tfs.naval.io

import tfs.naval.model.Ship

class FixedShipsReader extends ShipsReader {
  override def read: Vector[(String, Ship)] = Vector(
    ("MillenniumFalcon", List(
      (2, 5),
      (3, 5),
      (4, 5),
      (5, 5),
    )),
    ("Varyag", List(
      (9, 9)
    ))
  )
}
